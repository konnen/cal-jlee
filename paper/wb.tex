\section{Write Cache Design}
\label{sec:wb}

This section begins with a baseline design adopted from a previous work and then proceeds to presenting our proposed design.

\subsection{Baseline}
\label{sec:baseline}

We adopt the data structure from WOW~\cite{wow}, as illustrated in Figure~\ref{fig:wow}.
Throughout this paper, this data structure is called \textit{write queue}.
This data structure is based on classic algorithms, CLOCK~\cite{clock} and CSCAN~\cite{cscan}, to leverage both temporal and spatial locality.

\begin{figure}
\centering
\includegraphics[width=0.30\textwidth]{./fig/odg/wow.eps}
\caption{The baseline data structure that leverages both temporal and spatial locality, which is adopted from WOW~\cite{wow}. Throughout this paper, this data structure is called \textit{write queue}.}
\label{fig:wow}
\vspace{-\baselineskip}
\end{figure}

A write hit can be either a hit on a \textit{strip} or a hit on a \textit{stripe}.
A hit on a strip means there already exists the requested strip in the write cache.
Even if the requested strip does not exist, its write group could exist.
A hit on a stripe saves time to compute parity.
Upon write miss, a new write group is allocated if there is no corresponding write group (not a hit on a stripe).
If a hit on a stripe occurs, a new strip is allocated and the strip is stored in the existing, corresponding write group.
When a new write group is inserted, it should be done to keep the order of write groups by their logical block address (LBA).
Sorting write groups by their LBA leverages spatial locality.

When a destaging is necessary, the destage pointer advances.
It will be discussed soon to determine when to destage.
If the recency bit of the write group pointed by the destage pointer is zero, the write group is destaged.
If the recency bit is one, the write group is retained and the recency bit is cleared.
Then the destage pointer advances again until it finds a write group whose recency bit is zero.
The recency bit is set when the write group is hit.
The recency bit gives one more chance for hit write groups to survive for one more cycle, which leverages temporal locality.

WOW adopts a linear threshold scheduling~\cite{destage_algorithms_for_disk} to determine when to destage.
In said scheme, two thresholds are involved: low and high thresholds.
When the number of write groups stored in the write cache is below the low threshold, destaging is not triggered.
When the number exceeds the low threshold, destaging gets started.
The destaging rate increases proportionally to the number of write groups until it reaches the high threshold.
Exceeding the high threshold, the write groups are destaged at a full rate.

\subsection{Proposed Design}
\label{sec:proposed}

The proposed design is shown in Figure~\ref{fig:proposed}.
The main differences of the proposed design from WOW or traditional configuration illustrated in Figure~\ref{fig:write_cache} are as follows:
\begin{itemize}
\item The proposed design employs as many write queues as the number of SSDs in the array. Thus, an entry of each write queue is a \textit{strip}.
\item The destages from write queues are synchronized.
\item The write queues are placed after parity computation.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.25\textwidth]{./fig/odg/proposed.eps}
\caption{The proposed write cache for an array of SSDs.}
\label{fig:proposed}
\vspace{-\baselineskip}
\end{figure}

By employing multiple independent write queues, we can reduce the impact of GC on the write queue.
The write performance is degraded when the write queue is full.
While destage writes are delayed by GC, the chance of the write queue being full increases accordingly.
If only one write queue is placed as like in Figure~\ref{fig:write_cache}, the write queue cannot destage any more write group when any sub-request of the current destage write is delayed by GC.
In contrast, the proposed design allows other write queues to destage even if any write queue cannot destage because of GC.

The write queues are sychronized in that they are allowed to destage \textit{only if all the write queues have at least one strip to destage}.
The destage pointer advances independently, but when the pointer finds a strip to destage, it has to stop there until all the other pointers find one.
However, if the number of strips stored in the waiting write queue exceeds a predefined threshold, it is allowed to destage to prevent the waiting write queue from being full.
Except for those having no strip, the other write queues destage together.
This situation is referred to \textit{partial synchronization} whereas \textit{full synchronization} refers to the situation where every write queue participates in destaging.
For the threshold of the partial synchronization, the linear threshold scheduling~\cite{destage_algorithms_for_disk} is adopted.

The purpose of placing write queues after parity computation is to efficiently support the synchronization.
If they are placed before parity computation, a complicated algorithm is required to implement the synchronization.
The algorithm should select sub-requests as many as possible whose destination SSD is not overlapped.
It also needs to consider parity strip to be generated by parity computation.
Since such a selection algorithm is not trivial, its overhead could be comparable to parity computation.
We left it as our future work to devise a selection algorithm and to compare the current proposed design against the alternative architecture where the write queues are placed before parity computation.

The drawback of placing write queues after parity computation is that the delay taken by parity computation is always added to the response time of any requests.
In contrast, if the write queues are placed before parity computation, the delay is hidden when the cache hits.
However, this overhead is compensated for by improvement from the synchronization, as will be demonstrated by experiments.

The proposed design has no problem with the recovery from a disk failure.
If we destage every strip to SSDs except for the failed one, all the necessary data including the parity reside in other SSDs.
Therefore, the RAID controller can rebuild the lost data from other SSDs.
