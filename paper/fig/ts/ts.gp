set size 2.0, 1.0

set terminal postscript eps enhanced "Arial" 50
set boxwidth 0.9 relative
set style fill solid 1.0
set style fill solid border -1

set xlabel "Elapsed time (sec)"
set ylabel "Average response time" offset 1

set lmargin 4
set yrange[0:120]
set xrange[0:50]

set output "ts_wow.eps"
plot 'ts_wow' using ($2-$3)/1000:3 ti '' with impulses

set output "ts_align.eps"
plot 'ts_align' using ($2-$3)/1000:3 ti '' with impulses
