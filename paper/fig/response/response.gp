set size 1.5, 2.2

set terminal postscript eps enhanced "Arial" 50
set boxwidth 0.9 relative
set style fill solid 1.0
set style data histogram
set style histogram clustered gap 1
set style fill solid border -1
set xtics ("W_{small}" 0, "W_{medium}" 1, "W_{base}" 2, "W_{large}" 3, "W_{high}" 4, "W_{low}" 5, "W_{read}" 6, "W_{write}" 7)
set xtics rotate by -30
set key outside top horizontal reverse Left

set xlabel "Workloads"
set ylabel "Average response time" offset 1

set lmargin 4
set yrange[0:12]

set output "raid0.eps"
plot 'raid0.dat' using 2 ti 'WOW' fill pattern 3,\
 '' using 3 ti 'W/O Synchronization' fill pattern 4,\
 '' using 4 ti 'W/ Synchronization' fill pattern 8

set output "raid5.eps"
plot 'raid5.dat' using 2 ti 'WOW' fill pattern 3,\
 '' using 3 ti 'W/O Synchronization' fill pattern 4,\
 '' using 4 ti 'W/ Synchronization' fill pattern 8
