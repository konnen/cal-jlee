set size 1.5, 1.5

set terminal postscript eps enhanced "Arial" 50
set boxwidth 0.9 relative
set style fill solid 1.0
set style data histogram
set style histogram clustered gap 1
set style fill solid border -1
set xtics ("RAID5" 0, "RAID0" 1)
#set xtics rotate by -30
set key outside top horizontal reverse Left

#set xlabel "Workloads" offset 0,-1
set ylabel "Average response time" offset 1

set lmargin 4
set yrange[0:6]

set output "hpc.eps"
plot 'hpc.dat' using 2 ti 'WOW' fill pattern 3,\
 '' using 3 ti 'W/O Synchronization' fill pattern 4,\
 '' using 4 ti 'W/ Synchronization' fill pattern 8

set yrange[0:1.5]
set output "hpc_norm.eps"
plot 'hpc.dat' using ($2/$2) ti 'WOW' fill pattern 3,\
 '' using ($3/$2) ti 'W/O Synchronization' fill pattern 4,\
 '' using ($4/$2) ti 'W/ Synchronization' fill pattern 8
