set size 2.50, 2.00

set terminal postscript eps enhanced "Times new roman" 50
set boxwidth 0.9 relative
set style fill solid 1.0
set style data histogram
set style histogram clustered gap 1
#set style histogram rowstacked
set style fill solid border -1
set xtics ("W_{base}" 0, "W_{small}" 1, "W_{large}" 2, "W_{high}" 3, "W_{low}" 4, "W_{base}" 6, "W_{small}" 7, "W_{large}" 8, "W_{high}" 9, "W_{low}" 10)

#set xlabel "\n(a) Loop-level parallel benchmarks" offset character 0, -1, 0
set ylabel "Response time" offset 1
set key outside top horizontal center reverse Left

set output "compact.eps"

plot \
newhistogram "", \
'raid5.dat' using 2 ti "WOW" fill pattern 1,\
'' using 3 ti "W/O Synchronization" fill pattern 0,\
'' using 4 ti "W/ Synchronization" fill pattern 3,\
newhistogram "", \
'raid0.dat' using 2 ti "WOW" fill pattern 1,\
'' using 3 ti "W/O Synchronization" fill pattern 0,\
'' using 4 ti "W/ Synchronization" fill pattern 3
