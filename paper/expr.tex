\section{Experiments}
\label{sec:expr}

For evaluation of the proposed design, we employ DiskSim~\cite{disksim} and the SSD simulator developed by Microsoft Research (MSR)~\cite{msr_ssd}.
Detailed simulation parameters of an SSD are given in Table~\ref{tab:ssd_param}.
We use both RAID0 and RAID5 configurations where 8 and 9 SSDs are used respectively.
In RAID5, 8 SSDs are for data strips and one is for a parity strip whereas all of 8 SSDs are for data strips in RAID0.
Since the stripe unit is 128 KB, the 1 MB request constitutes a full stripe (128 KB $\times$ 8 = 1 MB).

We use nine synthetic workloads whose parameters are given in Table~\ref{tab:workload_param}.
$W_{hpc}$ is derived from the workload characterization of high-performance computing environment~\cite{jkim:pdsw:2010} and others are for break-down analysis of $W_{hpc}$.
The request size given in the table is its average and it follows the exponential distribution.
The inter-arrival time follows Poisson distribution.

\begin{table}
   \caption{\small SSD model parameters.}
   \label{tab:ssd_param}
 %\centering
 \scriptsize
  \begin{tabular}[m]{cc}
   \begin{minipage}{105pt}
    \begin{tabular}{|l|c|}
     \hline
     \multicolumn{2}{|c|}{SSD configuration} \\ \hline\hline
          Total capacity & 8 GB \\ \hline
          Reserved free blocks & 15 \% \\ \hline
          Minimum free blocks & 5 \% \\ \hline
          Cleaning policy & greedy\\ \hline
          Flash chip packages & 4\\ \hline
          Planes per package & 4 \\ \hline
          Blocks per plane & 512\\ \hline
          Pages per block & 64 \\ \hline
          Page size & 4 KB \\ \hline
     \end{tabular}
    \end{minipage}

    &

    \begin{minipage}{105pt}
     \begin{tabular}{|l|c|}
      \hline
      \multicolumn{2}{|c|}{Latency}\\\hline\hline
          Page read & 0.025 ms \\\hline
          Page write & 0.200 ms \\\hline
          Block erase & 1.5 ms \\\hline
        %  Copy back & disabled \\ \hline

	\multicolumn{2}{c}{} \\ 
	\multicolumn{2}{c}{} \\ \hline
      \multicolumn{2}{|c|}{RAID}\\\hline\hline
	Number of SSDs	& 8, 9	\\ \hline
	Redundancy	& RAID0, RAID5	\\ \hline
	Stripe unit	& 128 KB	\\ \hline
      \end{tabular}
     \end{minipage}
   \end{tabular}
\end{table}

\begin{table}
\caption{Workload parameters}
\label{tab:workload_param}
\centering
\scriptsize
\begin{tabular}{|l||rl|c|c|}
\hline
Workload		& \multicolumn{2}{c|}{Request size}	& Inter-arrival time	& Write percentage	\\ \hline
\hline
\multirow{4}{*}{$W_{hpc}$}		& 4 KB & 50\%		& \multirow{4}{*}{20 ms}	& \multirow{4}{*}{60 \%}			\\
			& 512 KB & 17\%	&			&			\\
			& 1 MB & 32\%	&			&			\\
			& 4 MB & 1\%	&			&			\\
\hline
\hline
$W_{small}$		& \multicolumn{2}{c|}{4 KB}		& 20 ms			& 60 \%			\\ \hline
$W_{medium}$		& \multicolumn{2}{c|}{512 KB}	& 20 ms			& 60 \%			\\ \hline
$W_{base}$		& \multicolumn{2}{c|}{1 MB}		& 20 ms			& 60 \%			\\ \hline
$W_{large}$		& \multicolumn{2}{c|}{4 MB}		& 20 ms			& 60 \%			\\ \hline
$W_{high}$		& \multicolumn{2}{c|}{1 MB}		& 10 ms			& 60 \%			\\ \hline
$W_{low}$		& \multicolumn{2}{c|}{1 MB}		& 40 ms			& 60 \%			\\ \hline
$W_{read}$		& \multicolumn{2}{c|}{1 MB}		& 20 ms			& 20 \%			\\ \hline
$W_{write}$		& \multicolumn{2}{c|}{1 MB}		& 20 ms			& 80 \%			\\ \hline
\end{tabular}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.25\textwidth]{./fig/response/hpc.eps}
\caption{Comparison of average response time for $W_{hpc}$.}
\label{fig:hpc}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.30\textwidth]{./fig/cdf/cdf.eps}
\caption{Cumulative distribution fuction of response time for RAID5.}
\label{fig:cdf}
\end{figure}

\begin{figure}
\centering
\scriptsize
\includegraphics[width=0.30\textwidth]{./fig/ts/ts_wow.eps} \\
(a) WOW \\
\includegraphics[width=0.30\textwidth]{./fig/ts/ts_align.eps} \\
(b) Synchronization
\caption{Time-series analysis of response time.}
\label{fig:ts}
\end{figure}

\begin{figure}
\centering
\scriptsize
\begin{tabular}{cc}
\includegraphics[width=0.24\textwidth]{./fig/response/raid5.eps} &
\includegraphics[width=0.24\textwidth]{./fig/response/raid0.eps} \\
(a) RAID5 &
(b) RAID0 \\
\end{tabular}
\caption{Performance evaluation results of the proposed write cache.}
\label{fig:response}
\end{figure}

By employing the proposed write cache and synchronization mechanism, the average response time is significantly reduced.
Figure~\ref{fig:hpc} compares the average response time of WOW, independent write queues without synchronization and with synchronization for $W_{hpc}$ workload.
If the write cache is split into multiple write queues and placed after parity computation but does not employ the synchronization mechanism, the average response time is rather increased compared with WOW.
Its response time is increased by 230.00\% and 233.03\% for RAID5 and RAID0, respectively.
Note that they are cut off from the graph because they are not of our interest.
However, the synchronization mechanism drastically improves the response time resulting in 15.74\% and 14.74\% improvement compared with WOW.

The synchronization mechanism defers destaging so that write queues can destage at the same time if they have a strip to destage.
It sometimes incurs additional delay which could affect the worst-case response time.
The cumulative distribution fuction (CDF) of response time (Figure~\ref{fig:cdf}) shows that the response time of the proposed write cache is smaller than that of WOW in most cases.
However, the zoom-in graph shows that CDF of the proposed write cache is more long-tailed, which means its worst-case response time is longer than WOW.
This also can be observed in the time-series analysis of response time as shown in Figure~\ref{fig:ts}.
We can see that there are more high peaks in the proposed write cache than in WOW.
Although the worst-case response time increases, the overall (average) response time is substantially reduced.

The response time of the proposed write cache is related with the request size.
Figure~\ref{fig:response}(a) shows the results of RAID5.
To begin with $W_{base}$, if we do not employ the synchronization, the average response time is increased by 29.98\% compared with WOW.
By employing the synchronization, the response time is reduced drastically resulting in 5.79\% improvement compared with WOW.
However, when the request size is small ($W_{small}$), the response time is not improved.
As discussed in Section~\ref{sec:wb}, this is due to the delay of parity computation.
When the request size is small, the delay cannot be compensated.
The response time is increased by 146.97\% even if the synchronization is employed.
Although the percentage looks very large, its absolute value is not.
The increment of response time is as small as 0.14 ms.
The main benefit comes from large requests ($W_{large}$).
Compared with WOW, the response time is reduced by 14.79\% when the synchronization is employed.
In the graph, the response time without synchronization, which is 30.03 ms, is cut off because it is out of interest.
Varying inter-arrival time ($W_{high}$ and $W_{low}$) and percentage of write requests ($W_{read}$ and $W_{write}$) does not have significant impact on the overall trend.

If the RAID configuration does not require parity computation (RAID0), parity computation does not incur additional delay.
Figure~\ref{fig:response}(b) shows the results of RAID0.
For $W_{small}$, the response time is increased by 12.05\% compared with WOW.
For $W_{base}$, $W_{large}$, the performance improvement is 32.51\% and 12.05\%, respectively.
