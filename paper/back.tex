\section{Background and Related Works}
\label{sec:back}

\subsection{Write cache for RAID}
\label{sec:write_cache}

Figure~\ref{fig:write_cache} illustrates a RAID controller that employs a write cache.
To cope with a sudden power fail, the write cache should be implemented with non-volatile memory~\cite{wow, stow}.
A read cache is not assumed in this paper.
As stated in previous works~\cite{wow, stow}, assuming no read cache prevents the performance gain by the write cache from being exaggerated.

\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{./fig/odg/write_cache.eps}
\caption{A RAID controller employing a write cache.}
\label{fig:write_cache}
\vspace{-\baselineskip}
\end{figure}

As long as there is free space in the write cache, an incoming write request is stored in the write cache and the write request is immediately committed to the requester.
The stored request is moved to the actual disks later in background.
This process is called \textit{destaging} and the write operations to the disk issued for destaging are called \textit{destage writes}.
A destage write is split into multiple \textit{strips}.
Depending on the RAID configuration, a parity strip may be added.
Each strip is stored in its corresponding disk.
The strips and the parity strip issued for a destage write are called \textit{stripe}.
The write requests belonging to a same stripe is called \textit{write group}.

To fully exploit benefits of a write cache, a cache controller should be designed to leverage temporal and spatial locality as well as to resist bursty writes~\cite{wow, stow}.
Temporal locality is leveraged by keeping hot data as much as possible.
On the other hand, to resist bursty writes, any stored data in the cache should be destaged as soon as possible so that there is enough room in the cache for the incoming requests.
Since these two objectives are often conflict, the cache controller should consider their trade-offs.
In the context of HDDs, leveraging spatial locality means minimizing mechanical movement inside a HDD.

\subsection{Characteristics of Solid-state Drives}
\label{sec:ssd}

Flash memory-based storage devices need an erase operation~\cite{Niijima95:IBMJournal} before over-writing new data over old data.
A write operation can be performed only to an erased page where an erase operation was performed before.
Since the erase operation is significantly slower than read or write operations, out-of-place writes are usually employed.
The out-of-place writes require garbage collection (GC) that cleans stale data to provide erased pages ready to be written.

GC is an important characteristic of an SSD that should be considered to design the write cache for an array of SSDs.
Page read usually takes 0.025 ms, page write takes 0.2 ms, and block erase takes 1.5 ms~\cite{msr_ssd}.
GC takes at least 1.5 ms because GC requires at least one erase operation.
In our experiments, we have empirically observed that GC takes 3 ms on average.
While GC is running, any incoming requests and destage writes cannot be serviced, which means that they could be delayed until GC completes.
Considering 3 ms is much larger than 0.025 ms and 0.2 ms, we can expect that GC has significant impact on the overall performance.

Therefore, to design a write cache for an array of SSDs, the write cache should be designed to avoid the impact of GC as much as possible.
Since GC significantly affects the performance of an array of SSDs, we advocate that avoiding the impact of GC should be given a high priority.

\subsection{Related Works}
\label{sec:related}

The cache controller should make two fundamental decisions: when to destage and what to destage.

There are two basic approaches to decide when to destage.
One approach is to destage in idle time~\cite{the_architecture_of_a}.
It destages dirty blocks as quickly as possible~\cite{stow}.
This approach gives higher priority on the resistance to bursty writes than the temporal locality.
On the other hand, the linear threshold scheduling~\cite{destage_algorithms_for_disk} balances those two objectives.
When the occupancy of the write cache is low, it destages slowly to exploit the locality.
Its destaging rate increases proportionally to the occupancy to avoid lack of free space.

What to destage is usually related with the spatial locality within an individual HDD.
Extensive research has been conducted on the scheduling algorithms to exploit the spatial locality in a HDD~\cite{haining:2000}.
For RAID controllers, WOW~\cite{wow} has been proposed that exploits both spatial locality and temporal locality at the same time.
One step further, STOW~\cite{stow} enhances WOW by separating random writes from sequential writes.

To the best of our knowledge, there has been no prior discussion on the write cache design for an array of SSDs.
In the domain of SSDs, GC has attracted researchers' interest because GC has significant impact on the overall performance of SSDs~\cite{gftl, rftl, Han:2006:IGC, jlee:ispass11, ykim:msst11}.
As for the array of SSDs, Harmonia~\cite{ykim:msst11} has been proposed to reduce the impact of GC by coordinating GCs of SSDs.
